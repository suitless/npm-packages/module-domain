import { INode } from "./INode";
import { IModule } from "./IModule";
import { IError } from "./IError";

/**
 * Constraint interface which defines constraints
 */
export interface IConstraint {
  /**
   * Name of the constraint
   */
  name: string;
  /**
   * Description of the constraint
   */
  description: string;
  /**
   * Wether the constraint is a global or a node constraint.
   */
  global: boolean;
  /**
   * Function to run a constraint
   * @param input IModule if global, INode otherwise.
   */
  run(input: any): IError[];
}
