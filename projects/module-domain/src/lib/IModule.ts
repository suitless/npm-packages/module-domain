import { INode } from "./INode";

/**
 * An IModule is a collection of INodes and their associated metadata.
 */
export interface IModule {
  /**
   * Returns the server generated Id if it exists, null if the IModule is not on the server.
   */
  id: string;
  /**
   * Returns the module version, which is to be used by the flowchart-walker.
   */
  version: number;
  /**
   * Returns the name of the module to be shown to the user.
   */
  name: string;
  /**
   * Returns the description of the module to be shown to the user.
   */
  description: string;
  /**
   * The disclaimer shown before the module.
   */
  disclaimer: string;
  /**
   * The disclaimer in the PDF report.
   */
  pdfDisclaimer: string;
  /**
   * Returns all tags that this module is searchable with.
   */
  tags: string[];
  /**
   * Returns the URI of the image that is associated to this module.
   */
  imageUri: string;
  /**
   * Returns all nodes that are in this IModule.
   */
  nodes: INode[];
  /**
   * Returns a boolean that determines if the module is public or private.
   */
  exposed: boolean;
  /**
   * Returns the owner of the module.
   */
  owner: string;
}
