/**
 * The errorType enum is a list of all unique warning types
 */
export enum EErrorType {
  Critical,
  Warning,
  Suggestion
}
