/**
 * Contains the data for a answer.
 */
export interface IAnswer {
  /**
   * Answer ID used for selecting the answer.
   */
  id: number;
  /**
   * The answer to be shown to the user.
   */
  text: string;
  /**
   * The optional image to be shown to the user.
   */
  image: string;
  /**
   * The to be followed flows.
   */
  flows: number[];
}
