/**
 * The report content is a encapsulating interface which contains
 * multiple section and a title which is top be shown in the index.
 */
export interface IReport {
  /**
   * Contains all sections in order.
   */
  content: IReportContent[];
  /**
   * The name of the module
   */
  moduleName: string;
  /**
   * The name of the user.
   */
  user: string;
  /**
   * The name of the user's business.
   */
  business: string;
}

/**
 * Defines a section in a report, such as a title, question, heading.
 */
export interface IReportContent {
  /**
   * The actual content of the report section.
   */
  text: string;
  /**
   * The type of the content.
   */
  type: EReportContentType;
}

export enum EReportContentType {
  Disclaimer,
  Question,
  Answer,
  Implication,
  Notification,
}
