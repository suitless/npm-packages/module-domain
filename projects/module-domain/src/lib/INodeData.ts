import { IAnswer } from "./IAnswer";

/**
 * Contains the nodeData for a start node.
 */
export interface IStartNodeData {
  /**
   * The route to be followed.
   */
  flows: number[];
  /**
   * Text to be shown to the user in at the start.
   */
  text: string;
  /**
   * The description of the module.
   */
  description: string;
}
/**
 * Contains the nodeData for an end node.
 */
export interface IEndNodeData {
  /**
   * Text to be shown when completing the module.
   */
  text: string;
  /**
   * Will force an ending to the module when it is encountered and set to true.
   */
  force: boolean;
}

/**
 * Contains the nodeData for a question node.
 */
export interface IQuestionNodeData {
  /**
   * The question to be shown to the user.
   */
  text: string;
  /**
   * The question explanations shown to the user.
   */
  explanations: string[];
  /**
   * The question examples shown to the user.
   */
  examples: string[];
  /**
   * The available answers.
   */
  answers: IAnswer[];
  /**
   * The optional image to be shown to the user.
   */
  image: string;
  /**
   * Whether or not the question is multiple choice
   */
  multi: boolean;
  /**
   * True if duplicate routes from answers should only be followed once.
   * False otherwise.
   */
  useUniqueRoutes: boolean;
}

/**
 * Contains the nodeData for a implication node.
 */
export interface IImplicationNodeData {
  /**
   * The route to be followed.
   */
  flows: number[];
  /**
   * Text to be shown to the user in the report/endscreen.
   */
  text: string;
  /**
   * Level to be used for the implication.
   */
  level: string;
}

/**
 * Contains the nodeData for a notification node.
 */
export interface INotificationNodeData {
  /**
   * The route to be followed.
   */
  flows: number[];
  /**
   * Text to be shown to the user in a notification.
   */
  text: string;
  /**
   * The optional image to be shown to the user.
   */
  image: string;
}

/**
 * Contains the nodeData for a compare node.
 */
export interface ICompareNodeData {
  /**
   * The A tag to be compared.
   * @todo: Create tag interface
   */
  tag: null;
  /**
   * The B tag to be compared.
   * @todo: Create tag interface
   */
  target: null;
  /**
   * This indicates wether a constant value or a tag is compared with
   * the base tag.
   */
  isTargetTag: boolean;

  /**
   * The routes to be followed when the compare results in greaterThan
   */
  greaterThan: number[];
  /**
   * The routes to be followed when the compare results in lesserThan
   */
  lesserThan: number[];
  /**
   * The routes to be followed when the compare results in an equal value
   */
  equals: number[];
}

/**
 * Contains the nodeData for an Alter node.
 */
export interface IAlterNodeData {
  /**
   * The route to be followed.
   */
  flows: number[];
  /**
   * The tag to be altered.
   * @todo: Create tag interface
   */
  tag: null;
  /**
   * The value to alter the tag with.
   */
  alterValue: number;
  /**
   * True if the tag and value should be added to each other.
   * False if the tag needs to be overwritten.
   */
  offsetMode: boolean;
}
