import { IReportContent } from "./IReport";
import { IModule } from "./IModule";
import { INode } from "./INode";

/**
 * Contains the state which is used while playing a module.
 */
export interface IState {
  /**
   * Contains the current module which is being used.
   */
  module: IModule;
  /**
   * Contains all queued op node ids which will be played next (in stack order).
   */
  stack: number[];
  /**
   * Contains the node which is currently being played.
   */
  currentNode: INode;
  /**
   * Contais a collection of all reportContent which can be used to generate
   * a report at the end.
   */
  reportContent: IReportContent[];
}
