/**
 * The nodeType enum is a list of all unique nodes
 */
export enum ENodeType {
  Start,
  End,
  Question,
  Implication,
  Notification,
  Compare,
  Alter
}
