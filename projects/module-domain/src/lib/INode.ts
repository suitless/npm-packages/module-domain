import { ENodeType } from "./ENodeType";
import {
  IStartNodeData,
  IQuestionNodeData,
  INotificationNodeData,
  IImplicationNodeData,
  IEndNodeData,
  ICompareNodeData,
  IAlterNodeData
} from "./INodeData";

/**
 * an INode is an instruction for the module-walker.
 * This class contains all data needed to execute the step and add its outcome to the stack.
 */
export interface INode {
  /**
   * The unique ID of this node.
   */
  id: number;
  /**
   * nodeType of this node.
   */
  type: ENodeType;
  /**
   * Typedata of this node.
   */
}

export interface IAnyNode extends INode {
  typeData: any;
}

export interface IStartNode extends INode {
  typeData: IStartNodeData;
}

export interface IQuestionNode extends INode {
  typeData: IQuestionNodeData;
}

export interface INotificationNode extends INode {
  typeData: INotificationNodeData;
}

export interface IImplicationNode extends INode {
  typeData: IImplicationNodeData;
}

export interface IEndNode extends INode {
  typeData: IEndNodeData;
}

export interface ICompareNode extends INode {
  typeData: ICompareNodeData;
}

export interface IAlterNode extends INode {
  typeData: IAlterNodeData;
}
